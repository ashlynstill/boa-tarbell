# README #
**note** I couldn't get this installed but needed to update some broken map tiles so I replaced the map.js file on the s3 server. If you are updating this please download map.js from s3 - Emily

This README documents the AJC news apps Battle of Atlanta anniversary interactive project, WAR IN OUR BACKYARDS, which lives [here](http://battleofatlanta.myajc.com)

### Built with ###
* HTML
* CSS
* Javascript/JQuery
* JQuery Zoom
* Mapbox/Leaflet
* d3.js
* Tarbell

####To bootstrap this project, refer to [tarbell documentation](http://tarbell.tribapps.com/) to get started.


### Contents ###
* Template files begin with underscores
* JS directory
* * app.js contains most of javascript
* * map.js contains all map related javascript
* CSS directory
* * base.css contains all main css
* * mobile.css mobile stylings for both tablet and phone


from tarbell directory, run local server with

    tarbell switch directory

using name of project directory


### Who do I talk to? ###

* Ashlyn Still - ashlyn [dot] still [at] ajc [dot] com